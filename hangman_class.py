import random

class hangman():

	def __init__(self,filename):
		self.lister = []
		self.f = open(filename,'r')
		self.lister = self.f.readlines()
		self.word = random.choice(self.lister)
		self.g_word = [] # guessed word list
		self.s_word = [] #real word to be guessed list
		self.hanglist = [
			' 	____________________\n 	|	||\n 	|	||\n 	|	||\n',	
				' 	|  _____||______\n	|  |           |\n	|  |   0  0    |\n	|  |           |\n	|  |   \__/    |\n	|   \_________/\n',
				' 	| \    |__|    /\n 	|  \___|__|___/\n	|      |__|\n	|      |__|\n	|      |__|\n	|      |__|\n',
				'	|     /    \ \n	|    /      \ \n	|   /        \ \n	| _/          \_ \n',
				''
			]
		for char in self.word:
			if(char!='\n'):	
				self.s_word.append(char)
				self.g_word.append('_ ')

		#self.chances = 0
 		#ip = '' #takes input from user
	def checkChar(self,char):
		self.count = 0
		flag = 0
		for ele in self.s_word:
			if ele == char:
				self.g_word[self.count] = char
				flag = 1
			self.count += 1
		if flag == 0:
			return 0	
		return self.g_word

	def play(self):
		
		chances = 0
		#print('Chances = '+ str(4) +"\n--start--")
		while(chances < 4):
			print('Chances Remaining = '+ str(4-chances))
			print('Word to be Guessed: ', end = '')
			print(''.join(self.g_word))
			print('Number of Letters in Word: ', len(self.word)-1)
			ip=input('Guess the letter: ')
			if ip in self.g_word:
				print('ALREADY GUESSED!!!')	
			elif ip not in 'abcdefghijklmnopqrstuvwxyz':
				print('ENTER AN APLPHABET')
			elif(self.checkChar(ip) == 0):
				chances+=1
				for i in range(chances):
					print(self.hanglist[i],end='')
				if chances==4:
					print('YOU LOSE!!!')
					print('CORRECT WORD IS:'+ ''.join(self.s_word))
					break
			else:
				self.g_word=self.checkChar(ip)
				#print(''.join(self.g_word))
				for i in range(chances):
					print(self.hanglist[i],end='')
				if '_ ' not in self.g_word :
					print('You have Won!!!')
					print('The word was: ' ,''.join(self.g_word))
					break

			print('______________________________________________________')


def start():
	print('--------------------------------------------------------------')
	print('                    Welcome to Hangman Game                   ')
	print('--------------------------------------------------------------')
	print('No of Chances you have to guess the word is: ',4)
	print('Lets Play:')
	print('--------------------------------------------------------------')
	choice = 'y'
	while(choice not in 'nN'):
		Hangman = hangman('Words.txt')
		Hangman.play()
		print('______________________________________________________')
		choice=input('CONTINUE?(Y or N)')
		print('______________________________________________________')
	


start()